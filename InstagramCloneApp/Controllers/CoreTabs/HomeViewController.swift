//
//  ViewController.swift
//  InstagramCloneApp
//
//  Created by admin on 08/05/2021.
//

import UIKit
import FirebaseAuth

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        checkLoginStatus()
    }
    
    private func checkLoginStatus() {
        // Check auth status
        if Auth.auth().currentUser == nil {
            // Show login screen
            let loginVC = LoginViewController()
            loginVC.modalPresentationStyle = .fullScreen
            present(loginVC, animated: false) {
                return
            }
        }
    }
}
